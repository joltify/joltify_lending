package keeper

import (
	"context"
	"errors"
	"time"

	storetypes "cosmossdk.io/store/types"
	types2 "github.com/cosmos/cosmos-sdk/codec/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/gogo/protobuf/proto"

	coserrors "cosmossdk.io/errors"
	sdkmath "cosmossdk.io/math"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/joltify-finance/joltify_lending/x/spv/types"
)

func (k Keeper) updateInterestData(rctx context.Context, interestData *types.BorrowInterest, reserve sdkmath.LegacyDec, firstBorrow bool, exchangeRatio sdkmath.LegacyDec) (sdk.Coin, time.Time, error) {
	var payment, paymentToInvestor sdk.Coin
	var thisPaymentTime time.Time
	ctx := sdk.UnwrapSDKContext(rctx)
	// as the payment cannot be happened at exact payfreq time, so we need to round down to the latest payment time
	latestPaymentDueTime := ctx.BlockTime().Truncate(time.Duration(interestData.PayFreq*BASE) * time.Second)

	latestPaymentTime := interestData.Payments[len(interestData.Payments)-1].PaymentTime
	if firstBorrow {
		if ctx.BlockTime().Before(latestPaymentTime.Add(time.Duration(interestData.PayFreq) * time.Second)) {
			return sdk.Coin{}, time.Time{}, errors.New("pay interest too early")
		}
	}
	delta := latestPaymentDueTime.Sub(latestPaymentTime)
	denom := interestData.Payments[0].PaymentAmount.Denom
	payment = sdk.NewCoin(denom, sdkmath.ZeroInt())
	lastBorrow := interestData.BorrowDetails[len(interestData.BorrowDetails)-1].BorrowedAmount
	if delta >= time.Second*time.Duration(interestData.PayFreq) {
		for delta > 0 {
			latestPaymentTime := interestData.Payments[len(interestData.Payments)-1].PaymentTime
			// we need to pay the whole month
			freqRatio := interestData.MonthlyRatio
			paymentAmount := freqRatio.Mul(sdkmath.LegacyNewDecFromInt(lastBorrow.Amount)).TruncateInt()
			if paymentAmount.IsZero() {
				return sdk.Coin{Denom: lastBorrow.Denom, Amount: sdkmath.ZeroInt()}, time.Time{}, nil
			}

			paymentAmountUsd := outboundConvertToUSD(paymentAmount, exchangeRatio)
			reservedAmount := sdkmath.LegacyNewDecFromInt(paymentAmountUsd).Mul(reserve).TruncateInt()
			toInvestors := paymentAmountUsd.Sub(reservedAmount)
			pReserve, found := k.GetReserve(ctx, denom)
			if !found {
				k.SetReserve(ctx, sdk.NewCoin(denom, reservedAmount))
			} else {
				pReserve = pReserve.AddAmount(reservedAmount)
				k.SetReserve(ctx, pReserve)
			}
			paymentToInvestor = sdk.NewCoin(denom, toInvestors)
			thisPayment := sdk.NewCoin(denom, paymentAmountUsd)
			thisPaymentTime = latestPaymentTime.Add(time.Duration(interestData.PayFreq*BASE) * time.Second).Truncate(time.Duration(interestData.PayFreq*BASE) * time.Second)

			// since the spv may not pay the interest at exact next payment circle, we need to adjust it here
			currentPayment := types.PaymentItem{PaymentTime: thisPaymentTime, PaymentAmount: paymentToInvestor, BorrowedAmount: lastBorrow}
			interestData.Payments = append(interestData.Payments, &currentPayment)
			interestData.AccInterest = interestData.AccInterest.Add(paymentToInvestor)
			delta = latestPaymentDueTime.Sub(thisPaymentTime)
			payment = payment.AddAmount(thisPayment.Amount)
		}
		return payment, thisPaymentTime, nil

	} else {
		latestPaymentTime := interestData.Payments[len(interestData.Payments)-1].PaymentTime
		currentTimeTruncated := ctx.BlockTime().Truncate(time.Duration(interestData.PayFreq) * time.Second)
		if currentTimeTruncated.Before(latestPaymentTime) {
			return sdk.Coin{Denom: lastBorrow.Denom, Amount: sdkmath.ZeroInt()}, time.Time{}, nil
		}
		deltaTruncated := currentTimeTruncated.Sub(latestPaymentTime).Seconds()
		r := CalculateInterestRate(interestData.Apy, int(interestData.PayFreq))
		interest := r.Power(uint64(deltaTruncated)).Sub(sdkmath.LegacyOneDec())

		usdInterest := interest.Mul(exchangeRatio)
		paymentAmountUsd := usdInterest.MulInt(lastBorrow.Amount).TruncateInt()
		reservedAmountUsd := sdkmath.LegacyNewDecFromInt(paymentAmountUsd).Mul(reserve).TruncateInt()
		toInvestors := paymentAmountUsd.Sub(reservedAmountUsd)

		pReserve, found := k.GetReserve(ctx, denom)
		if !found {
			k.SetReserve(ctx, sdk.NewCoin(denom, reservedAmountUsd))
		} else {
			pReserve = pReserve.AddAmount(reservedAmountUsd)
			k.SetReserve(ctx, pReserve)
		}
		paymentToInvestor = sdk.NewCoin(denom, toInvestors)
		payment = sdk.NewCoin(denom, paymentAmountUsd)
		thisPaymentTime = latestPaymentTime.Add(time.Duration(interestData.PayFreq*BASE) * time.Second).Truncate(time.Duration(interestData.PayFreq*BASE) * time.Second)

		// since the spv may not pay the interest at exact next payment circle, we need to adjust it here
		currentPayment := types.PaymentItem{PaymentTime: thisPaymentTime, PaymentAmount: paymentToInvestor, BorrowedAmount: lastBorrow}
		interestData.Payments = append(interestData.Payments, &currentPayment)
		interestData.AccInterest = interestData.AccInterest.Add(paymentToInvestor)
		return payment, thisPaymentTime, nil

	}
}

// getAllinterestToBePaid returns the total interest to be paid for all the borrows in the pool using the
// LOCAL currency
func (k Keeper) getAllInterestToBePaid(ctx context.Context, poolInfo *types.PoolInfo) (sdkmath.Int, time.Time, error) {
	nftClasses := poolInfo.PoolNFTIds
	// the first element is the pool class, we skip it
	totalPayment := sdkmath.NewInt(0)
	firstBorrow := true
	var exchangeRatio sdkmath.LegacyDec

	if poolInfo.PoolStatus == types.PoolInfo_INACTIVE {
		return sdkmath.ZeroInt(), time.Time{}, errors.New("no interest to be paid")
	}

	if poolInfo.InterestPrepayment == nil || poolInfo.InterestPrepayment.Counter == 0 {
		a, _ := denomConvertToLocalAndUsd(poolInfo.BorrowedAmount.Denom)
		price, err := k.priceFeedKeeper.GetCurrentPrice(ctx, denomConvertToMarketID(a))
		if err != nil {
			panic(err)
		}
		exchangeRatio = price.Price
	} else {
		exchangeRatio = poolInfo.InterestPrepayment.ExchangeRatio
		poolInfo.InterestPrepayment.Counter--
		if poolInfo.InterestPrepayment.Counter == 0 {
			poolInfo.InterestPrepayment = nil
		}
	}
	var poolLatestPaymentTime time.Time
	for _, el := range nftClasses {
		class, found := k.NftKeeper.GetClass(ctx, el)
		if !found {
			panic(found)
		}
		var borrowInterest types.BorrowInterest
		var err error
		err = proto.Unmarshal(class.Data.Value, &borrowInterest)
		if err != nil {
			panic(err)
		}
		thisBorrowInterest, thisNFTPaymentTime, err := k.updateInterestData(ctx, &borrowInterest, poolInfo.ReserveFactor, firstBorrow, exchangeRatio)
		if err != nil {
			return sdkmath.Int{}, time.Time{}, err
		}
		if thisBorrowInterest.Amount.IsZero() {
			continue
		}
		class.Data, err = types2.NewAnyWithValue(&borrowInterest)
		if err != nil {
			panic("pack class any data failed")
		}
		err = k.NftKeeper.UpdateClass(ctx, class)
		if err != nil {
			return sdkmath.Int{}, time.Time{}, err
		}
		totalPayment = totalPayment.Add(thisBorrowInterest.Amount)
		firstBorrow = false
		if thisNFTPaymentTime.After(poolLatestPaymentTime) {
			poolLatestPaymentTime = thisNFTPaymentTime
		}
	}
	return totalPayment, poolLatestPaymentTime, nil
}

func (k msgServer) calculatePaymentMonth(rctx context.Context, poolInfo types.PoolInfo, marketId string, totalPaid sdkmath.Int) (int32, sdkmath.Int, sdkmath.Int, sdkmath.LegacyDec, error) {
	ctx := sdk.UnwrapSDKContext(rctx)
	paymentAmount, err := k.calculateTotalDueInterest(ctx, poolInfo)
	if err != nil {
		return 0, sdkmath.ZeroInt(), sdkmath.ZeroInt(), sdkmath.LegacyZeroDec(), err
	}
	if paymentAmount.IsZero() {
		return 0, sdkmath.ZeroInt(), sdkmath.ZeroInt(), sdkmath.LegacyZeroDec(), errors.New("no interest to be paid")
	}
	usdEachMonth, ratio, err := k.outboundConvertToUSDWithMarketID(ctx, marketId, paymentAmount)
	if err != nil {
		return 0, sdkmath.ZeroInt(), sdkmath.ZeroInt(), sdkmath.LegacyZeroDec(), err
	}
	counter := totalPaid.Quo(usdEachMonth)
	return int32(counter.Int64()), usdEachMonth.Mul(counter), usdEachMonth, ratio, nil
}

func (k msgServer) RepayInterest(rctx context.Context, msg *types.MsgRepayInterest) (*types.MsgRepayInterestResponse, error) {
	ctx := sdk.UnwrapSDKContext(rctx)
	ctx = ctx.WithGasMeter(storetypes.NewInfiniteGasMeter())

	spvAddress, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return nil, coserrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid address %v", msg.Creator)
	}

	poolInfo, found := k.GetPools(ctx, msg.PoolIndex)
	if !found {
		return nil, coserrors.Wrapf(types.ErrPoolNotFound, "pool %v not found", msg.PoolIndex)
	}
	if poolInfo.PoolStatus == types.PoolInfo_FROZEN || poolInfo.PoolStatus == types.PoolInfo_INACTIVE || poolInfo.PoolStatus == types.PoolInfo_FREEZING {
		return nil, types.ErrPoolNotActive
	}

	if msg.Token.Denom != poolInfo.TargetAmount.Denom {
		return nil, coserrors.Wrapf(types.ErrInconsistencyToken, "pool denom %v and repay is %v", poolInfo.TargetAmount.Denom, msg.Token.Denom)
	}

	if poolInfo.BorrowedAmount.IsZero() || msg.Token.Amount.IsZero() {
		return nil, coserrors.Wrapf(types.ErrInvalidParameter, "borrow amount is zero, no interest to be paid or interest paid is zero")
	}

	if poolInfo.InterestPrepayment != nil {
		return nil, coserrors.Wrapf(types.ErrInvalidParameter, "we have the prepayment interest, not accepting new interest payment")
	}

	// we have enough interest in the escrow account
	if !poolInfo.EscrowInterestAmount.IsNegative() {
		a, _ := denomConvertToLocalAndUsd(poolInfo.BorrowedAmount.Denom)
		marketID := denomConvertToMarketID(a)
		counter, interestReceived, eachMonth, ratio, err := k.calculatePaymentMonth(ctx, poolInfo, marketID, msg.Token.Amount)
		if err != nil {
			return nil, coserrors.Wrapf(err, "calculate payment month failed")
		}

		if counter < 1 {
			return nil, coserrors.Wrapf(types.ErrInsufficientFund, "you must pay at least one interest cycle (%v)", eachMonth)
		}

		poolInfo.EscrowInterestAmount = poolInfo.EscrowInterestAmount.Add(interestReceived)
		prepayment := types.InterestPrepayment{
			Counter:       counter,
			ExchangeRatio: ratio,
		}

		poolInfo.InterestPrepayment = &prepayment

		err = k.bankKeeper.SendCoinsFromAccountToModule(ctx, spvAddress, types.ModuleAccount, sdk.Coins{sdk.NewCoin(msg.Token.Denom, interestReceived)})
		if err != nil {
			return nil, coserrors.Wrapf(err, "fail to transfer the repayment from spv to module")
		}

		k.SetPool(ctx, poolInfo)
		return &types.MsgRepayInterestResponse{}, nil
	}

	// leftover := poolInfo.EscrowInterestAmount.Add(msg.Token.Amount)
	ownInterest := poolInfo.EscrowInterestAmount.Abs()
	leftover := msg.Token.Amount.Sub(ownInterest)
	if leftover.IsNegative() {
		return nil, coserrors.Wrapf(types.ErrInsufficientFund, "you must pay all the outstanding interest which is %v", ownInterest)
	}

	a, _ := denomConvertToLocalAndUsd(poolInfo.BorrowedAmount.Denom)
	marketID := denomConvertToMarketID(a)
	counter, interestReceived, eachMonthPayment, ratio, err := k.calculatePaymentMonth(ctx, poolInfo, marketID, leftover)
	if err != nil {
		return nil, coserrors.Wrapf(err, "calculate payment each month failed")
	}

	if counter < 1 {
		return nil, coserrors.Wrapf(types.ErrInsufficientFund, "you must pay at least one interest cycle (%v)", eachMonthPayment)
	}
	poolInfo.EscrowInterestAmount = interestReceived
	prepayment := types.InterestPrepayment{
		Counter:       counter,
		ExchangeRatio: ratio,
	}

	poolInfo.InterestPrepayment = &prepayment
	totalGetFromSPV := ownInterest.Add(interestReceived)

	paidOutInterest := sdk.NewCoin(msg.Token.Denom, totalGetFromSPV)
	err = k.bankKeeper.SendCoinsFromAccountToModule(ctx, spvAddress, types.ModuleAccount, sdk.Coins{paidOutInterest})
	if err != nil {
		return nil, coserrors.Wrapf(err, "fail to transfer the repayment from spv to module")
	}

	ctx.EventManager().EmitEvent(
		sdk.NewEvent(
			types.EventTypeRepayInterestSPV,
			sdk.NewAttribute(types.AttributeCreator, msg.Creator),
			sdk.NewAttribute(types.AttributeAmount, paidOutInterest.String()),
		),
	)

	k.SetPool(ctx, poolInfo)
	return &types.MsgRepayInterestResponse{InterestAmount: paidOutInterest}, nil
}
