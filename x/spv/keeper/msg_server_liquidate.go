package keeper

import (
	"context"

	coserrors "cosmossdk.io/errors"
	sdkmath "cosmossdk.io/math"
	types2 "github.com/cosmos/cosmos-sdk/codec/types"

	storetypes "cosmossdk.io/store/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/gogo/protobuf/proto"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/joltify-finance/joltify_lending/x/spv/types"
)

func (k Keeper) doUpdateLiquidationInfo(rctx context.Context, el string, amountFromLiquidator, totalPoolBorrowed sdk.Coin, paidAmount sdkmath.Int) (sdkmath.Int, error) {
	ctx := sdk.UnwrapSDKContext(rctx)
	class, found := k.NftKeeper.GetClass(ctx, el)
	if !found {
		panic(found)
	}
	var borrowInterest types.BorrowInterest
	var err error
	err = proto.Unmarshal(class.Data.Value, &borrowInterest)
	if err != nil {
		panic(err)
	}

	lastBorrow := borrowInterest.BorrowDetails[len(borrowInterest.BorrowDetails)-1].BorrowedAmount
	if paidAmount.IsZero() {
		paidAmount = sdkmath.LegacyNewDecFromInt(amountFromLiquidator.Amount).Mul(sdkmath.LegacyNewDecFromInt(lastBorrow.Amount)).Quo(sdkmath.LegacyNewDecFromInt(totalPoolBorrowed.Amount)).TruncateInt()
	}

	if borrowInterest.LiquidationItems == nil {
		borrowInterest.LiquidationItems = make([]*types.LiquidationItem, 1, 10)
		borrowInterest.LiquidationItems[0] = &types.LiquidationItem{
			Amount:                 sdk.NewCoin(amountFromLiquidator.Denom, paidAmount),
			LiquidationPaymentTime: ctx.BlockTime(),
		}
	} else {
		borrowInterest.LiquidationItems = append(borrowInterest.LiquidationItems, &types.LiquidationItem{
			Amount:                 sdk.NewCoin(amountFromLiquidator.Denom, paidAmount),
			LiquidationPaymentTime: ctx.BlockTime(),
		})
	}

	class.Data, err = types2.NewAnyWithValue(&borrowInterest)
	if err != nil {
		panic("pack class any data failed")
	}
	err = k.NftKeeper.UpdateClass(ctx, class)
	if err != nil {
		return sdkmath.ZeroInt(), err
	}

	return paidAmount, nil
}

func (k Keeper) handleLiquidation(ctx context.Context, poolInfo types.PoolInfo, amount sdk.Coin) error {
	nftClasses := poolInfo.PoolNFTIds
	totalBorrowed := poolInfo.BorrowedAmount
	// the first element is the pool class, we skip it
	totalPaid := sdkmath.ZeroInt()
	for i, el := range nftClasses {
		if i == 0 {
			// we will handle the fist element later
			continue
		}
		amountPaid, err := k.doUpdateLiquidationInfo(ctx, el, amount, totalBorrowed, sdkmath.ZeroInt())
		if err != nil {
			return err
		}
		totalPaid = totalPaid.Add(amountPaid)
	}

	if len(nftClasses) > 0 {
		paidAmount := amount.Amount.Sub(totalPaid)
		_, err := k.doUpdateLiquidationInfo(ctx, nftClasses[0], amount, totalBorrowed, paidAmount)
		if err != nil {
			return err
		}
	}
	poolInfo.TotalLiquidationAmount = poolInfo.TotalLiquidationAmount.Add(amount.Amount)
	k.SetPool(ctx, poolInfo)

	return nil
}

func (k msgServer) Liquidate(rctx context.Context, msg *types.MsgLiquidate) (*types.MsgLiquidateResponse, error) {
	ctx := sdk.UnwrapSDKContext(rctx)

	ctx = ctx.WithGasMeter(storetypes.NewInfiniteGasMeter())

	liquidator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return nil, coserrors.Wrapf(sdkerrors.ErrInvalidAddress, "invalid address %v", msg.Creator)
	}

	if msg.Amount.IsZero() {
		return nil, coserrors.Wrapf(sdkerrors.ErrInvalidRequest, "the amount cannot be zero")
	}

	poolInfo, found := k.GetPools(ctx, msg.PoolIndex)
	if !found {
		return nil, coserrors.Wrapf(types.ErrPoolNotFound, "pool cannot be found %v", msg.PoolIndex)
	}

	if msg.Amount.Denom != poolInfo.TargetAmount.Denom {
		return nil, coserrors.Wrapf(types.ErrInconsistencyToken, "the token is not the same as the borrowed token %v", msg.Amount.Denom)
	}

	if poolInfo.PoolStatus != types.PoolInfo_Liquidation {
		return nil, coserrors.Wrapf(types.ErrPoolNotInLiquidation, "pool is not in liquidation %v", poolInfo.PoolStatus)
	}

	err = k.bankKeeper.SendCoinsFromAccountToModule(ctx, liquidator, types.ModuleAccount, sdk.NewCoins(msg.Amount))
	if err != nil {
		return nil, coserrors.Wrapf(types.ErrLiquidation, "liquidation failed %v", err)
	}

	err = k.handleLiquidation(ctx, poolInfo, msg.Amount)
	if err != nil {
		return nil, coserrors.Wrapf(types.ErrLiquidation, "liquidation failed %v", err)
	}

	return &types.MsgLiquidateResponse{}, nil
}
