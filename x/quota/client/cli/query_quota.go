package cli

import (
	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/joltify-finance/joltify_lending/x/quota/types"
	"github.com/spf13/cobra"
)

func CmdListQuota() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "list-quota",
		Short: "list all quota",
		RunE: func(cmd *cobra.Command, args []string) error {
			clientCtx, err := client.GetClientQueryContext(cmd)
			if err != nil {
				return err
			}

			pageReq, err := client.ReadPageRequest(cmd.Flags())
			if err != nil {
				return err
			}

			queryClient := types.NewQueryClient(clientCtx)

			params := &types.QueryAllQuotaRequest{
				Pagination: pageReq,
			}

			res, err := queryClient.QuotaAll(cmd.Context(), params)
			if err != nil {
				return err
			}

			return clientCtx.PrintProto(res)
		},
	}

	flags.AddPaginationFlagsToCmd(cmd, cmd.Use)
	flags.AddQueryFlagsToCmd(cmd)

	return cmd
}

func CmdShowQuota() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "show-quota [id]",
		Short: "shows a quota",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			clientCtx, err := client.GetClientQueryContext(cmd)
			if err != nil {
				return err
			}

			queryClient := types.NewQueryClient(clientCtx)

			moduleName := args[0]

			params := &types.QueryGetQuotaRequest{
				QuotaModuleName: moduleName,
			}

			res, err := queryClient.Quota(cmd.Context(), params)
			if err != nil {
				return err
			}

			return clientCtx.PrintProto(res)
		},
	}

	flags.AddQueryFlagsToCmd(cmd)

	return cmd
}

func CmdShowAccountQuota() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "show-account-quota [id]",
		Short: "shows account quota",
		Args:  cobra.ExactArgs(2),
		RunE: func(cmd *cobra.Command, args []string) error {
			clientCtx, err := client.GetClientQueryContext(cmd)
			if err != nil {
				return err
			}

			queryClient := types.NewQueryClient(clientCtx)

			moduleName := args[0]
			accountAddr := args[1]
			params := &types.QueryGetAccountQuotaRequest{
				QuotaModuleName: moduleName,
				AccountAddress:  accountAddr,
			}

			res, err := queryClient.AccountQuota(cmd.Context(), params)
			if err != nil {
				return err
			}

			return clientCtx.PrintProto(res)
		},
	}

	flags.AddQueryFlagsToCmd(cmd)

	return cmd
}
