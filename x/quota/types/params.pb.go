// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: joltify/quota/params.proto

package types

import (
	fmt "fmt"
	github_com_cosmos_cosmos_sdk_types "github.com/cosmos/cosmos-sdk/types"
	types "github.com/cosmos/cosmos-sdk/types"
	_ "github.com/cosmos/gogoproto/gogoproto"
	proto "github.com/cosmos/gogoproto/proto"
	io "io"
	math "math"
	math_bits "math/bits"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion3 // please upgrade the proto package

type Target struct {
	ModuleName    string                                   `protobuf:"bytes,1,opt,name=module_name,json=moduleName,proto3" json:"module_name,omitempty"`
	CoinsSum      github_com_cosmos_cosmos_sdk_types.Coins `protobuf:"bytes,2,rep,name=CoinsSum,proto3,castrepeated=github.com/cosmos/cosmos-sdk/types.Coins" json:"CoinsSum"`
	HistoryLength int64                                    `protobuf:"varint,3,opt,name=history_length,json=historyLength,proto3" json:"history_length,omitempty" yaml:"history_length"`
}

func (m *Target) Reset()         { *m = Target{} }
func (m *Target) String() string { return proto.CompactTextString(m) }
func (*Target) ProtoMessage()    {}
func (*Target) Descriptor() ([]byte, []int) {
	return fileDescriptor_d5999d940b1e3d04, []int{0}
}
func (m *Target) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *Target) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_Target.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *Target) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Target.Merge(m, src)
}
func (m *Target) XXX_Size() int {
	return m.Size()
}
func (m *Target) XXX_DiscardUnknown() {
	xxx_messageInfo_Target.DiscardUnknown(m)
}

var xxx_messageInfo_Target proto.InternalMessageInfo

func (m *Target) GetModuleName() string {
	if m != nil {
		return m.ModuleName
	}
	return ""
}

func (m *Target) GetCoinsSum() github_com_cosmos_cosmos_sdk_types.Coins {
	if m != nil {
		return m.CoinsSum
	}
	return nil
}

func (m *Target) GetHistoryLength() int64 {
	if m != nil {
		return m.HistoryLength
	}
	return 0
}

type WhiteList struct {
	ModuleName  string   `protobuf:"bytes,1,opt,name=moduleName,proto3" json:"moduleName,omitempty"`
	AddressList []string `protobuf:"bytes,2,rep,name=addressList,proto3" json:"addressList,omitempty"`
}

func (m *WhiteList) Reset()         { *m = WhiteList{} }
func (m *WhiteList) String() string { return proto.CompactTextString(m) }
func (*WhiteList) ProtoMessage()    {}
func (*WhiteList) Descriptor() ([]byte, []int) {
	return fileDescriptor_d5999d940b1e3d04, []int{1}
}
func (m *WhiteList) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *WhiteList) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_WhiteList.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *WhiteList) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WhiteList.Merge(m, src)
}
func (m *WhiteList) XXX_Size() int {
	return m.Size()
}
func (m *WhiteList) XXX_DiscardUnknown() {
	xxx_messageInfo_WhiteList.DiscardUnknown(m)
}

var xxx_messageInfo_WhiteList proto.InternalMessageInfo

func (m *WhiteList) GetModuleName() string {
	if m != nil {
		return m.ModuleName
	}
	return ""
}

func (m *WhiteList) GetAddressList() []string {
	if m != nil {
		return m.AddressList
	}
	return nil
}

type BanList struct {
	ModuleName  string   `protobuf:"bytes,1,opt,name=moduleName,proto3" json:"moduleName,omitempty"`
	AddressList []string `protobuf:"bytes,2,rep,name=addressList,proto3" json:"addressList,omitempty"`
}

func (m *BanList) Reset()         { *m = BanList{} }
func (m *BanList) String() string { return proto.CompactTextString(m) }
func (*BanList) ProtoMessage()    {}
func (*BanList) Descriptor() ([]byte, []int) {
	return fileDescriptor_d5999d940b1e3d04, []int{2}
}
func (m *BanList) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *BanList) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_BanList.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *BanList) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BanList.Merge(m, src)
}
func (m *BanList) XXX_Size() int {
	return m.Size()
}
func (m *BanList) XXX_DiscardUnknown() {
	xxx_messageInfo_BanList.DiscardUnknown(m)
}

var xxx_messageInfo_BanList proto.InternalMessageInfo

func (m *BanList) GetModuleName() string {
	if m != nil {
		return m.ModuleName
	}
	return ""
}

func (m *BanList) GetAddressList() []string {
	if m != nil {
		return m.AddressList
	}
	return nil
}

// Params defines the parameters for the module.
type Params struct {
	Targets           []*Target    `protobuf:"bytes,1,rep,name=targets,proto3" json:"targets,omitempty"`
	PerAccounttargets []*Target    `protobuf:"bytes,2,rep,name=PerAccounttargets,proto3" json:"PerAccounttargets,omitempty"`
	Whitelist         []*WhiteList `protobuf:"bytes,3,rep,name=whitelist,proto3" json:"whitelist,omitempty"`
	Banlist           []*BanList   `protobuf:"bytes,4,rep,name=banlist,proto3" json:"banlist,omitempty"`
}

func (m *Params) Reset()      { *m = Params{} }
func (*Params) ProtoMessage() {}
func (*Params) Descriptor() ([]byte, []int) {
	return fileDescriptor_d5999d940b1e3d04, []int{3}
}
func (m *Params) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *Params) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_Params.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *Params) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Params.Merge(m, src)
}
func (m *Params) XXX_Size() int {
	return m.Size()
}
func (m *Params) XXX_DiscardUnknown() {
	xxx_messageInfo_Params.DiscardUnknown(m)
}

var xxx_messageInfo_Params proto.InternalMessageInfo

func (m *Params) GetTargets() []*Target {
	if m != nil {
		return m.Targets
	}
	return nil
}

func (m *Params) GetPerAccounttargets() []*Target {
	if m != nil {
		return m.PerAccounttargets
	}
	return nil
}

func (m *Params) GetWhitelist() []*WhiteList {
	if m != nil {
		return m.Whitelist
	}
	return nil
}

func (m *Params) GetBanlist() []*BanList {
	if m != nil {
		return m.Banlist
	}
	return nil
}

func init() {
	proto.RegisterType((*Target)(nil), "joltify.quota.Target")
	proto.RegisterType((*WhiteList)(nil), "joltify.quota.WhiteList")
	proto.RegisterType((*BanList)(nil), "joltify.quota.BanList")
	proto.RegisterType((*Params)(nil), "joltify.quota.Params")
}

func init() { proto.RegisterFile("joltify/quota/params.proto", fileDescriptor_d5999d940b1e3d04) }

var fileDescriptor_d5999d940b1e3d04 = []byte{
	// 456 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xac, 0x53, 0x3f, 0x6f, 0x13, 0x31,
	0x14, 0x3f, 0x37, 0x55, 0x42, 0x1c, 0x15, 0x09, 0x8b, 0xa2, 0x6b, 0x06, 0x5f, 0x94, 0x29, 0x4b,
	0xed, 0x16, 0x24, 0x84, 0x3a, 0x41, 0x3a, 0x52, 0x50, 0x65, 0x90, 0x90, 0x58, 0x2a, 0xdf, 0x9d,
	0x7b, 0x31, 0x9c, 0xed, 0x70, 0xf6, 0x01, 0xf9, 0x16, 0x8c, 0x8c, 0xcc, 0x7c, 0x92, 0x8e, 0x1d,
	0x18, 0x98, 0x0a, 0x4a, 0x76, 0x06, 0x3e, 0x01, 0x3a, 0xdf, 0x5d, 0x49, 0x0b, 0x62, 0x62, 0xba,
	0xf3, 0xfb, 0xfd, 0x91, 0xdf, 0xef, 0x3d, 0xc3, 0xe1, 0x2b, 0x93, 0x3b, 0x79, 0xba, 0xa0, 0x6f,
	0x4a, 0xe3, 0x38, 0x9d, 0xf3, 0x82, 0x2b, 0x4b, 0xe6, 0x85, 0x71, 0x06, 0x6d, 0x35, 0x18, 0xf1,
	0xd8, 0xf0, 0x76, 0x66, 0x32, 0xe3, 0x11, 0x5a, 0xfd, 0xd5, 0xa4, 0x21, 0x4e, 0x8c, 0x55, 0xc6,
	0xd2, 0x98, 0x5b, 0x41, 0xdf, 0xee, 0xc7, 0xc2, 0xf1, 0x7d, 0x9a, 0x18, 0xa9, 0x6b, 0x7c, 0xfc,
	0x05, 0xc0, 0xee, 0x73, 0x5e, 0x64, 0xc2, 0xa1, 0x08, 0x0e, 0x94, 0x49, 0xcb, 0x5c, 0x9c, 0x68,
	0xae, 0x44, 0x08, 0x46, 0x60, 0xd2, 0x67, 0xb0, 0x2e, 0x3d, 0xe5, 0x4a, 0xa0, 0x0c, 0xde, 0x38,
	0x34, 0x52, 0xdb, 0x67, 0xa5, 0x0a, 0x37, 0x46, 0x9d, 0xc9, 0xe0, 0xee, 0x0e, 0xa9, 0xed, 0x49,
	0x65, 0x4f, 0x1a, 0x7b, 0x52, 0x91, 0xa6, 0x7b, 0x67, 0x17, 0x51, 0xf0, 0xf9, 0x5b, 0x34, 0xc9,
	0xa4, 0x9b, 0x95, 0x31, 0x49, 0x8c, 0xa2, 0xcd, 0x5d, 0xea, 0xcf, 0xae, 0x4d, 0x5f, 0x53, 0xb7,
	0x98, 0x0b, 0xeb, 0x05, 0x96, 0x5d, 0x9a, 0xa3, 0x87, 0xf0, 0xe6, 0x4c, 0x5a, 0x67, 0x8a, 0xc5,
	0x49, 0x2e, 0x74, 0xe6, 0x66, 0x61, 0x67, 0x04, 0x26, 0x9d, 0xe9, 0xce, 0xcf, 0x8b, 0x68, 0x7b,
	0xc1, 0x55, 0x7e, 0x30, 0xbe, 0x8a, 0x8f, 0xd9, 0x56, 0x53, 0x38, 0xaa, 0xcf, 0x4f, 0x60, 0xff,
	0xc5, 0x4c, 0x3a, 0x71, 0x24, 0xad, 0x43, 0x18, 0xae, 0x75, 0xf1, 0x97, 0xbe, 0x46, 0x70, 0xc0,
	0xd3, 0xb4, 0x10, 0xd6, 0x56, 0x74, 0xdf, 0x5a, 0x9f, 0xad, 0x97, 0xc6, 0x8f, 0x61, 0x6f, 0xca,
	0xf5, 0x7f, 0x32, 0xfb, 0x01, 0x60, 0xf7, 0xd8, 0x0f, 0x12, 0x51, 0xd8, 0x73, 0x3e, 0x7c, 0x1b,
	0x02, 0x1f, 0xe8, 0x36, 0xb9, 0x32, 0x54, 0x52, 0x8f, 0x86, 0xb5, 0x2c, 0x74, 0x08, 0x6f, 0x1d,
	0x8b, 0xe2, 0x51, 0x92, 0x98, 0x52, 0xbb, 0x56, 0xba, 0xf1, 0x2f, 0xe9, 0x9f, 0x7c, 0x74, 0x1f,
	0xf6, 0xdf, 0x55, 0xe1, 0xe4, 0xd5, 0x05, 0x3b, 0x5e, 0x1c, 0x5e, 0x13, 0x5f, 0x86, 0xc7, 0x7e,
	0x53, 0xd1, 0x1e, 0xec, 0xc5, 0x5c, 0x7b, 0xd5, 0xa6, 0x57, 0xdd, 0xb9, 0xa6, 0x6a, 0x32, 0x62,
	0x2d, 0xed, 0x60, 0xf3, 0xe3, 0xa7, 0x28, 0x98, 0xb2, 0xb3, 0x25, 0x06, 0xe7, 0x4b, 0x0c, 0xbe,
	0x2f, 0x31, 0xf8, 0xb0, 0xc2, 0xc1, 0xf9, 0x0a, 0x07, 0x5f, 0x57, 0x38, 0x78, 0xf9, 0x60, 0x6d,
	0x39, 0x1a, 0xab, 0xdd, 0x53, 0xa9, 0xb9, 0x4e, 0x44, 0x7b, 0xae, 0x26, 0x9c, 0x4a, 0x9d, 0xd1,
	0xf7, 0xcd, 0x1b, 0xf0, 0x2b, 0x13, 0x77, 0xfd, 0xfa, 0xde, 0xfb, 0x15, 0x00, 0x00, 0xff, 0xff,
	0xb7, 0x54, 0x38, 0x2e, 0x21, 0x03, 0x00, 0x00,
}

func (m *Target) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *Target) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *Target) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if m.HistoryLength != 0 {
		i = encodeVarintParams(dAtA, i, uint64(m.HistoryLength))
		i--
		dAtA[i] = 0x18
	}
	if len(m.CoinsSum) > 0 {
		for iNdEx := len(m.CoinsSum) - 1; iNdEx >= 0; iNdEx-- {
			{
				size, err := m.CoinsSum[iNdEx].MarshalToSizedBuffer(dAtA[:i])
				if err != nil {
					return 0, err
				}
				i -= size
				i = encodeVarintParams(dAtA, i, uint64(size))
			}
			i--
			dAtA[i] = 0x12
		}
	}
	if len(m.ModuleName) > 0 {
		i -= len(m.ModuleName)
		copy(dAtA[i:], m.ModuleName)
		i = encodeVarintParams(dAtA, i, uint64(len(m.ModuleName)))
		i--
		dAtA[i] = 0xa
	}
	return len(dAtA) - i, nil
}

func (m *WhiteList) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *WhiteList) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *WhiteList) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if len(m.AddressList) > 0 {
		for iNdEx := len(m.AddressList) - 1; iNdEx >= 0; iNdEx-- {
			i -= len(m.AddressList[iNdEx])
			copy(dAtA[i:], m.AddressList[iNdEx])
			i = encodeVarintParams(dAtA, i, uint64(len(m.AddressList[iNdEx])))
			i--
			dAtA[i] = 0x12
		}
	}
	if len(m.ModuleName) > 0 {
		i -= len(m.ModuleName)
		copy(dAtA[i:], m.ModuleName)
		i = encodeVarintParams(dAtA, i, uint64(len(m.ModuleName)))
		i--
		dAtA[i] = 0xa
	}
	return len(dAtA) - i, nil
}

func (m *BanList) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *BanList) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *BanList) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if len(m.AddressList) > 0 {
		for iNdEx := len(m.AddressList) - 1; iNdEx >= 0; iNdEx-- {
			i -= len(m.AddressList[iNdEx])
			copy(dAtA[i:], m.AddressList[iNdEx])
			i = encodeVarintParams(dAtA, i, uint64(len(m.AddressList[iNdEx])))
			i--
			dAtA[i] = 0x12
		}
	}
	if len(m.ModuleName) > 0 {
		i -= len(m.ModuleName)
		copy(dAtA[i:], m.ModuleName)
		i = encodeVarintParams(dAtA, i, uint64(len(m.ModuleName)))
		i--
		dAtA[i] = 0xa
	}
	return len(dAtA) - i, nil
}

func (m *Params) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *Params) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *Params) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	if len(m.Banlist) > 0 {
		for iNdEx := len(m.Banlist) - 1; iNdEx >= 0; iNdEx-- {
			{
				size, err := m.Banlist[iNdEx].MarshalToSizedBuffer(dAtA[:i])
				if err != nil {
					return 0, err
				}
				i -= size
				i = encodeVarintParams(dAtA, i, uint64(size))
			}
			i--
			dAtA[i] = 0x22
		}
	}
	if len(m.Whitelist) > 0 {
		for iNdEx := len(m.Whitelist) - 1; iNdEx >= 0; iNdEx-- {
			{
				size, err := m.Whitelist[iNdEx].MarshalToSizedBuffer(dAtA[:i])
				if err != nil {
					return 0, err
				}
				i -= size
				i = encodeVarintParams(dAtA, i, uint64(size))
			}
			i--
			dAtA[i] = 0x1a
		}
	}
	if len(m.PerAccounttargets) > 0 {
		for iNdEx := len(m.PerAccounttargets) - 1; iNdEx >= 0; iNdEx-- {
			{
				size, err := m.PerAccounttargets[iNdEx].MarshalToSizedBuffer(dAtA[:i])
				if err != nil {
					return 0, err
				}
				i -= size
				i = encodeVarintParams(dAtA, i, uint64(size))
			}
			i--
			dAtA[i] = 0x12
		}
	}
	if len(m.Targets) > 0 {
		for iNdEx := len(m.Targets) - 1; iNdEx >= 0; iNdEx-- {
			{
				size, err := m.Targets[iNdEx].MarshalToSizedBuffer(dAtA[:i])
				if err != nil {
					return 0, err
				}
				i -= size
				i = encodeVarintParams(dAtA, i, uint64(size))
			}
			i--
			dAtA[i] = 0xa
		}
	}
	return len(dAtA) - i, nil
}

func encodeVarintParams(dAtA []byte, offset int, v uint64) int {
	offset -= sovParams(v)
	base := offset
	for v >= 1<<7 {
		dAtA[offset] = uint8(v&0x7f | 0x80)
		v >>= 7
		offset++
	}
	dAtA[offset] = uint8(v)
	return base
}
func (m *Target) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	l = len(m.ModuleName)
	if l > 0 {
		n += 1 + l + sovParams(uint64(l))
	}
	if len(m.CoinsSum) > 0 {
		for _, e := range m.CoinsSum {
			l = e.Size()
			n += 1 + l + sovParams(uint64(l))
		}
	}
	if m.HistoryLength != 0 {
		n += 1 + sovParams(uint64(m.HistoryLength))
	}
	return n
}

func (m *WhiteList) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	l = len(m.ModuleName)
	if l > 0 {
		n += 1 + l + sovParams(uint64(l))
	}
	if len(m.AddressList) > 0 {
		for _, s := range m.AddressList {
			l = len(s)
			n += 1 + l + sovParams(uint64(l))
		}
	}
	return n
}

func (m *BanList) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	l = len(m.ModuleName)
	if l > 0 {
		n += 1 + l + sovParams(uint64(l))
	}
	if len(m.AddressList) > 0 {
		for _, s := range m.AddressList {
			l = len(s)
			n += 1 + l + sovParams(uint64(l))
		}
	}
	return n
}

func (m *Params) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	if len(m.Targets) > 0 {
		for _, e := range m.Targets {
			l = e.Size()
			n += 1 + l + sovParams(uint64(l))
		}
	}
	if len(m.PerAccounttargets) > 0 {
		for _, e := range m.PerAccounttargets {
			l = e.Size()
			n += 1 + l + sovParams(uint64(l))
		}
	}
	if len(m.Whitelist) > 0 {
		for _, e := range m.Whitelist {
			l = e.Size()
			n += 1 + l + sovParams(uint64(l))
		}
	}
	if len(m.Banlist) > 0 {
		for _, e := range m.Banlist {
			l = e.Size()
			n += 1 + l + sovParams(uint64(l))
		}
	}
	return n
}

func sovParams(x uint64) (n int) {
	return (math_bits.Len64(x|1) + 6) / 7
}
func sozParams(x uint64) (n int) {
	return sovParams(uint64((x << 1) ^ uint64((int64(x) >> 63))))
}
func (m *Target) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowParams
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: Target: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: Target: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field ModuleName", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowParams
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthParams
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthParams
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.ModuleName = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 2:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field CoinsSum", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowParams
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthParams
			}
			postIndex := iNdEx + msglen
			if postIndex < 0 {
				return ErrInvalidLengthParams
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.CoinsSum = append(m.CoinsSum, types.Coin{})
			if err := m.CoinsSum[len(m.CoinsSum)-1].Unmarshal(dAtA[iNdEx:postIndex]); err != nil {
				return err
			}
			iNdEx = postIndex
		case 3:
			if wireType != 0 {
				return fmt.Errorf("proto: wrong wireType = %d for field HistoryLength", wireType)
			}
			m.HistoryLength = 0
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowParams
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				m.HistoryLength |= int64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
		default:
			iNdEx = preIndex
			skippy, err := skipParams(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if (skippy < 0) || (iNdEx+skippy) < 0 {
				return ErrInvalidLengthParams
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *WhiteList) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowParams
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: WhiteList: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: WhiteList: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field ModuleName", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowParams
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthParams
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthParams
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.ModuleName = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 2:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field AddressList", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowParams
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthParams
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthParams
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.AddressList = append(m.AddressList, string(dAtA[iNdEx:postIndex]))
			iNdEx = postIndex
		default:
			iNdEx = preIndex
			skippy, err := skipParams(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if (skippy < 0) || (iNdEx+skippy) < 0 {
				return ErrInvalidLengthParams
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *BanList) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowParams
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: BanList: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: BanList: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field ModuleName", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowParams
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthParams
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthParams
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.ModuleName = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
		case 2:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field AddressList", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowParams
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthParams
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthParams
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.AddressList = append(m.AddressList, string(dAtA[iNdEx:postIndex]))
			iNdEx = postIndex
		default:
			iNdEx = preIndex
			skippy, err := skipParams(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if (skippy < 0) || (iNdEx+skippy) < 0 {
				return ErrInvalidLengthParams
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *Params) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowParams
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: Params: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: Params: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Targets", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowParams
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthParams
			}
			postIndex := iNdEx + msglen
			if postIndex < 0 {
				return ErrInvalidLengthParams
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Targets = append(m.Targets, &Target{})
			if err := m.Targets[len(m.Targets)-1].Unmarshal(dAtA[iNdEx:postIndex]); err != nil {
				return err
			}
			iNdEx = postIndex
		case 2:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field PerAccounttargets", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowParams
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthParams
			}
			postIndex := iNdEx + msglen
			if postIndex < 0 {
				return ErrInvalidLengthParams
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.PerAccounttargets = append(m.PerAccounttargets, &Target{})
			if err := m.PerAccounttargets[len(m.PerAccounttargets)-1].Unmarshal(dAtA[iNdEx:postIndex]); err != nil {
				return err
			}
			iNdEx = postIndex
		case 3:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Whitelist", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowParams
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthParams
			}
			postIndex := iNdEx + msglen
			if postIndex < 0 {
				return ErrInvalidLengthParams
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Whitelist = append(m.Whitelist, &WhiteList{})
			if err := m.Whitelist[len(m.Whitelist)-1].Unmarshal(dAtA[iNdEx:postIndex]); err != nil {
				return err
			}
			iNdEx = postIndex
		case 4:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field Banlist", wireType)
			}
			var msglen int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowParams
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				msglen |= int(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if msglen < 0 {
				return ErrInvalidLengthParams
			}
			postIndex := iNdEx + msglen
			if postIndex < 0 {
				return ErrInvalidLengthParams
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.Banlist = append(m.Banlist, &BanList{})
			if err := m.Banlist[len(m.Banlist)-1].Unmarshal(dAtA[iNdEx:postIndex]); err != nil {
				return err
			}
			iNdEx = postIndex
		default:
			iNdEx = preIndex
			skippy, err := skipParams(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if (skippy < 0) || (iNdEx+skippy) < 0 {
				return ErrInvalidLengthParams
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func skipParams(dAtA []byte) (n int, err error) {
	l := len(dAtA)
	iNdEx := 0
	depth := 0
	for iNdEx < l {
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return 0, ErrIntOverflowParams
			}
			if iNdEx >= l {
				return 0, io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= (uint64(b) & 0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		wireType := int(wire & 0x7)
		switch wireType {
		case 0:
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowParams
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				iNdEx++
				if dAtA[iNdEx-1] < 0x80 {
					break
				}
			}
		case 1:
			iNdEx += 8
		case 2:
			var length int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowParams
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				length |= (int(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if length < 0 {
				return 0, ErrInvalidLengthParams
			}
			iNdEx += length
		case 3:
			depth++
		case 4:
			if depth == 0 {
				return 0, ErrUnexpectedEndOfGroupParams
			}
			depth--
		case 5:
			iNdEx += 4
		default:
			return 0, fmt.Errorf("proto: illegal wireType %d", wireType)
		}
		if iNdEx < 0 {
			return 0, ErrInvalidLengthParams
		}
		if depth == 0 {
			return iNdEx, nil
		}
	}
	return 0, io.ErrUnexpectedEOF
}

var (
	ErrInvalidLengthParams        = fmt.Errorf("proto: negative length found during unmarshaling")
	ErrIntOverflowParams          = fmt.Errorf("proto: integer overflow")
	ErrUnexpectedEndOfGroupParams = fmt.Errorf("proto: unexpected end of group")
)
