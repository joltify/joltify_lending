package types

// Events emitted by the incentive module
const (
	EventTypeClaim          = "claim_reward"
	AttributeKeyClaimedBy   = "claimed_by"
	AttributeKeyClaimAmount = "claim_amount"
	AttributeKeyClaimType   = "claim_type"
)
