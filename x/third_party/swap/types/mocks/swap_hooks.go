// Code generated by mockery 2.7.4. DO NOT EDIT.

package mocks

import (
	"context"

	"github.com/cosmos/cosmos-sdk/types"

	sdkmath "cosmossdk.io/math"
	mock "github.com/stretchr/testify/mock"
)

// SwapHooks is an autogenerated mock type for the SwapHooks type
type SwapHooks struct {
	mock.Mock
}

// AfterPoolDepositCreated provides a mock function with given fields: ctx, poolID, depositor, sharedOwned
func (_m *SwapHooks) AfterPoolDepositCreated(ctx context.Context, poolID string, depositor types.AccAddress, sharedOwned sdkmath.Int) {
	_m.Called(ctx, poolID, depositor, sharedOwned)
}

// BeforePoolDepositModified provides a mock function with given fields: ctx, poolID, depositor, sharedOwned
func (_m *SwapHooks) BeforePoolDepositModified(ctx context.Context, poolID string, depositor types.AccAddress, sharedOwned sdkmath.Int) {
	_m.Called(ctx, poolID, depositor, sharedOwned)
}
