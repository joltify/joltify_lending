package keeper

import (
	"context"
	"fmt"
	"strconv"
	"testing"

	"cosmossdk.io/store/metrics"

	sdkmath "cosmossdk.io/math"
	types2 "github.com/joltify-finance/joltify_lending/x/third_party/pricefeed/types"

	"github.com/gogo/protobuf/proto"

	"cosmossdk.io/x/nft"
	authtypes "github.com/cosmos/cosmos-sdk/x/auth/types"
	kycmoduletypes "github.com/joltify-finance/joltify_lending/x/kyc/types"

	"cosmossdk.io/log"
	"cosmossdk.io/store"
	storetypes "cosmossdk.io/store/types"
	tmproto "github.com/cometbft/cometbft/proto/tendermint/types"
	dbm "github.com/cosmos/cosmos-db"
	"github.com/cosmos/cosmos-sdk/codec"
	codectypes "github.com/cosmos/cosmos-sdk/codec/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	typesparams "github.com/cosmos/cosmos-sdk/x/params/types"
	"github.com/joltify-finance/joltify_lending/x/spv/keeper"
	"github.com/joltify-finance/joltify_lending/x/spv/types"
	"github.com/stretchr/testify/require"
)

const (
	oneWeek  = 7 * 24 * 3600
	oneMonth = oneWeek * 4
	oneYear  = oneWeek * 52
)

type mockKycKeeper struct{}

var Wallets = []string{
	"jolt1kkujrm0lqeu0e5va5f6mmwk87wva0k8cmam8jq",
	"jolt166yyvsypvn6cwj2rc8sme4dl6v0g62hn3862kl",
	"jolt1z0y0zl0trsnuqmqf5v034pyv9sp39jg3rv6lsm",
	"jolt1fcaa73cc9c2l3l2u57skddgd0zm749ncukx90g",
	"jolt1ut358ywu78ztkt5m90dwmklz79rwau6vs8vhlp",
	"jolt1v9ls99c83dst7x6xwwnsjcyp5zsa3acfhaxq5n",
	"jolt169a92jz2rmxy0ll73kztlmtucswvvft78xeqne",
	"jolt13xxls80rw3p036zyfy8hhtjyvft4ckg5a09agh",
}

func (m mockKycKeeper) GetInvestorWallets(_ context.Context, investorID string) (kycmoduletypes.Investor, error) {
	return kycmoduletypes.Investor{InvestorId: investorID, WalletAddress: Wallets}, nil
}

func (m mockKycKeeper) GetProject(ctx context.Context, index int32) (val kycmoduletypes.ProjectInfo, found bool) {
	b := kycmoduletypes.BasicInfo{
		Description:    "This is the test info",
		ProjectsUrl:    "empty",
		ProjectCountry: "ABC",
		BusinessNumber: "ABC123",
		Reserved:       []byte("reserved"),
		ProjectName:    "This is the test info",
		Email:          "email address",
		Name:           "example.com",
	}

	acc, _ := sdk.AccAddressFromBech32("jolt1txtsnx4gr4effr8542778fsxc20j5vzqxet7t0")
	pi1 := kycmoduletypes.ProjectInfo{
		Index:            1,
		SPVName:          "defaultSPV",
		ProjectOwner:     acc,
		BasicInfo:        &b,
		ProjectLength:    31536000, // 1 year
		PayFreq:          "15768000",
		BaseApy:          sdkmath.LegacyNewDecWithPrec(12, 2),
		MarketId:         "aud:usd",
		SeparatePool:     false,
		JuniorMinRatio:   sdkmath.LegacyNewDecWithPrec(1, 15),
		MinBorrowAmount:  sdkmath.NewInt(2000000),
		MinDepositAmount: sdkmath.NewInt(1000000),
	}

	b2 := kycmoduletypes.BasicInfo{
		Description:    "This is the test info2",
		ProjectsUrl:    "empty2",
		ProjectCountry: "ABC2",
		BusinessNumber: "ABC123-2",
		Reserved:       []byte("reserved"),
		ProjectName:    "This is the test info2",
		Email:          "example.com2",
		Name:           "email address2",
	}

	pi2 := kycmoduletypes.ProjectInfo{
		Index:            2,
		SPVName:          "defaultSPV2",
		ProjectOwner:     acc,
		BasicInfo:        &b2,
		ProjectLength:    31536000, // 1 year
		PayFreq:          "15768000",
		BaseApy:          sdkmath.LegacyNewDecWithPrec(12, 2),
		MarketId:         "aud:usd",
		SeparatePool:     false,
		JuniorMinRatio:   sdkmath.LegacyNewDecWithPrec(1, 15),
		MinBorrowAmount:  sdkmath.NewInt(200000000000000),
		MinDepositAmount: sdkmath.NewInt(10000000000000),
	}

	pi3 := kycmoduletypes.ProjectInfo{
		Index:            3,
		SPVName:          "defaultSPV3",
		ProjectOwner:     acc,
		BasicInfo:        &b2,
		ProjectLength:    oneYear, // 1 year
		PayFreq:          strconv.Itoa(oneMonth),
		BaseApy:          sdkmath.LegacyNewDecWithPrec(12, 2),
		MarketId:         "aud:usd",
		SeparatePool:     false,
		JuniorMinRatio:   sdkmath.LegacyNewDecWithPrec(1, 15),
		MinBorrowAmount:  sdkmath.NewInt(200000000000000),
		MinDepositAmount: sdkmath.NewInt(5000000000000),
	}

	pi4 := kycmoduletypes.ProjectInfo{
		Index:            4,
		SPVName:          "defaultSPV3",
		ProjectOwner:     acc,
		BasicInfo:        &b2,
		ProjectLength:    oneYear, // 1 year
		PayFreq:          strconv.Itoa(oneWeek),
		BaseApy:          sdkmath.LegacyNewDecWithPrec(10, 2),
		MarketId:         "aud:usd",
		SeparatePool:     false,
		JuniorMinRatio:   sdkmath.LegacyNewDecWithPrec(1, 15),
		MinBorrowAmount:  sdkmath.NewInt(200000000000000),
		MinDepositAmount: sdkmath.NewInt(100000000000000),
	}

	pi5 := kycmoduletypes.ProjectInfo{
		Index:            5,
		SPVName:          "defaultSPV5",
		ProjectOwner:     acc,
		BasicInfo:        &b2,
		ProjectLength:    oneYear, // 1 year
		PayFreq:          strconv.Itoa(oneWeek),
		BaseApy:          sdkmath.LegacyNewDecWithPrec(10, 2),
		MarketId:         "aud:usd",
		SeparatePool:     false,
		JuniorMinRatio:   sdkmath.LegacyNewDecWithPrec(1, 15),
		MinBorrowAmount:  sdkmath.NewInt(200000000000000),
		MinDepositAmount: sdkmath.NewInt(150000000000000),
	}

	a := []*kycmoduletypes.ProjectInfo{&pi1, &pi2, &pi3, &pi4, &pi5}
	if index > int32(len(a)) {
		return kycmoduletypes.ProjectInfo{}, false
	}
	return *a[index-1], true
}

func (m mockKycKeeper) GetByWallet(_ context.Context, wallet string) (kycmoduletypes.Investor, error) {
	inv := kycmoduletypes.Investor{
		InvestorId:    "1",
		WalletAddress: []string{wallet},
	}

	inv2 := kycmoduletypes.Investor{InvestorId: "2", WalletAddress: Wallets}

	for _, el := range Wallets {
		if wallet == el {
			return inv2, nil
		}
	}

	return inv, nil
}

type mockAccKeeper struct{}

func (m mockAccKeeper) GetAccount(ctx context.Context, addr sdk.AccAddress) sdk.AccountI {
	// TODO implement me
	panic("implement me")
}

func (m mockAccKeeper) GetModuleAccount(ctx context.Context, name string) sdk.ModuleAccountI {
	addr := authtypes.NewModuleAddress(name)
	baseAcc := authtypes.NewBaseAccountWithAddress(addr)
	return authtypes.NewModuleAccount(baseAcc, name, "mint")
}

func (m mockAccKeeper) GetModuleAddress(name string) sdk.AccAddress {
	// TODO implement me
	panic("implement me")
}

type mockNFTKeeper struct {
	classes         map[string]*nft.Class
	nfts            map[string]*nft.NFT
	nftsWithClassID map[string]*nft.NFT
}

func (m mockNFTKeeper) GetOwner(ctx context.Context, classID string, nftID string) sdk.AccAddress {
	// TODO implement me
	return []byte("owner")
}

func (m mockNFTKeeper) Burn(ctx context.Context, classID string, nftID string) error {
	key := fmt.Sprintf("%v:%v", classID, nftID)
	delete(m.nftsWithClassID, key)
	return nil
}

func (m mockNFTKeeper) Transfer(ctx context.Context, classID string, nftID string, receiver sdk.AccAddress) error {
	panic("implement me")
}

func (m mockNFTKeeper) GetTotalSupply(ctx context.Context, classID string) uint64 {
	counter := 0
	for k := range m.nftsWithClassID {
		if classID == k {
			counter++
		}
	}
	return uint64(counter)
}

func (m mockNFTKeeper) GetNFT(ctx context.Context, classID, nftID string) (nft.NFT, bool) {
	key := fmt.Sprintf("%v:%v", classID, nftID)
	thisNft, found := m.nftsWithClassID[key]
	if !found {
		return nft.NFT{}, false
	}

	bz, err := proto.Marshal(thisNft)
	if err != nil {
		panic(err)
	}

	var returnNFT nft.NFT
	err = proto.Unmarshal(bz, &returnNFT)
	if err != nil {
		panic(err)
	}
	return returnNFT, true
}

func (m mockNFTKeeper) Update(ctx context.Context, nftToken nft.NFT) error {
	key := fmt.Sprintf("%v:%v", nftToken.ClassId, nftToken.Id)
	m.nftsWithClassID[key] = &nftToken

	return nil
}

func (m mockNFTKeeper) Mint(ctx context.Context, nft nft.NFT, receiver sdk.AccAddress) error {
	m.nfts[receiver.String()] = &nft
	key := fmt.Sprintf("%v:%v", nft.ClassId, nft.Id)
	m.nftsWithClassID[key] = &nft
	return nil
}

func (m mockNFTKeeper) SaveClass(ctx context.Context, class nft.Class) error {
	m.classes[class.Id] = &class
	return nil
}

func (m mockNFTKeeper) UpdateClass(ctx context.Context, class nft.Class) error {
	// var borrowInterest types.BorrowInterest
	// err := proto.Unmarshal(class.Data.Value, &borrowInterest)
	// if err != nil {
	//	panic(err)
	// }
	m.classes[class.Id] = &class
	return nil
}

func (m mockNFTKeeper) GetClass(ctx context.Context, classID string) (nft.Class, bool) {
	r, ok := m.classes[classID]
	if !ok {
		return nft.Class{}, ok
	}
	return *r, ok
}

type mockbankKeeper struct {
	BankData map[string]sdk.Coins
}

func (m mockbankKeeper) SendCoinsFromModuleToModule(ctx context.Context, senderModule, recipientModule string, amt sdk.Coins) error {
	m.BankData[recipientModule] = m.BankData[recipientModule].Add(amt...)
	return nil
}

func (m mockbankKeeper) SendCoinsFromAccountToModule(ctx context.Context, senderAddr sdk.AccAddress, recipientModule string, amt sdk.Coins) error {
	addr := authtypes.NewModuleAddress(recipientModule)
	m.BankData[addr.String()] = m.BankData[addr.String()].Add(amt...)
	return nil
}

func (m mockbankKeeper) SendCoinsFromModuleToAccount(ctx context.Context, senderModule string, recipientAddr sdk.AccAddress, amt sdk.Coins) error {
	// addr := authtypes.NewModuleAddress(senderModule)
	m.BankData[recipientAddr.String()] = m.BankData[recipientAddr.String()].Add(amt...)
	// m.BankData[addr.String()]= m.BankData[addr.String()].Sub(amt...)
	return nil
}

func (m mockbankKeeper) GetSupply(ctx context.Context, denom string) sdk.Coin {
	// TODO implement me
	panic("implement me")
}

func (m mockbankKeeper) GetBalance(ctx context.Context, addr sdk.AccAddress, denom string) sdk.Coin {
	coins, ok := m.BankData[addr.String()]
	if !ok {
		panic("address cannot be found")
	}

	ok, coin := coins.Find(denom)
	if !ok {
		panic("denom cannot be found")
	}

	return coin
}

func (m mockbankKeeper) GetAllBalances(ctx context.Context, addr sdk.AccAddress) sdk.Coins {
	coins := sdk.NewCoins()
	for _, v := range m.BankData[addr.String()] {
		coins = coins.Add(v)
	}
	return coins
}

func (m mockbankKeeper) SpendableCoins(ctx context.Context, addr sdk.AccAddress) sdk.Coins {
	// TODO implement me
	panic("implement me")
}

func (m mockbankKeeper) BurnCoins(ctx context.Context, name string, amt sdk.Coins) error {
	// TODO implement me
	panic("implement me")
}

type mockPriceFeedKeeper struct{}

type MockAuctionKeeper struct {
	AuctionAmount []sdk.Coin
	SellerBid     []string
	mockbank      *mockbankKeeper
}

func (m MockAuctionKeeper) StartSurplusAuction(ctx context.Context, seller string, lot sdk.Coin, bidDenom string) (uint64, error) {
	m.AuctionAmount[0] = lot
	m.SellerBid[0] = seller
	m.SellerBid[1] = bidDenom

	moduleAddr := authtypes.NewModuleAddress(seller)
	if m.mockbank != nil {
		m.mockbank.BankData[moduleAddr.String()] = m.mockbank.BankData[moduleAddr.String()].Sub(lot)
	}
	return 1, nil
}

func (m mockPriceFeedKeeper) GetCurrentPrice(ctx context.Context, marketID string) (types2.CurrentPrice, error) {
	return types2.CurrentPrice{MarketID: "aud:usd", Price: sdkmath.LegacyMustNewDecFromStr("0.7")}, nil
}

type FakeIncentiveKeeper struct {
	poolIncentive map[string]sdk.Coins
}

func (f FakeIncentiveKeeper) GetPoolIncentive() map[string]sdk.Coins {
	return f.poolIncentive
}

func (f FakeIncentiveKeeper) SetSPVRewardTokens(ctx context.Context, poolId string, rewardTokens sdk.Coins) {
	f.poolIncentive[poolId] = rewardTokens
}

type fakeSPVFunctions struct{}

func (f fakeSPVFunctions) AfterSPVInterestPaid(ctx context.Context, poolID string, interestPaid sdkmath.Int) {
}

func (f fakeSPVFunctions) BeforeNFTBurned(ctx context.Context, poolIndex, investorID string, linkednfts []string) error {
	return nil
}

func SpvKeeper(t testing.TB) (*keeper.Keeper, types.NFTKeeper, types.BankKeeper, MockAuctionKeeper, FakeIncentiveKeeper, context.Context) {
	storeKey := storetypes.NewKVStoreKey(types.StoreKey)
	memStoreKey := storetypes.NewMemoryStoreKey(types.MemStoreKey)

	db := dbm.NewMemDB()
	stateStore := store.NewCommitMultiStore(db, log.NewNopLogger(), metrics.NewNoOpMetrics())
	stateStore.MountStoreWithDB(storeKey, storetypes.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(memStoreKey, storetypes.StoreTypeMemory, nil)
	require.NoError(t, stateStore.LoadLatestVersion())

	registry := codectypes.NewInterfaceRegistry()
	cdc := codec.NewProtoCodec(registry)

	paramsSubspace := typesparams.NewSubspace(cdc,
		types.Amino,
		storeKey,
		memStoreKey,
		"SpvParams",
	)
	kycKeeper := mockKycKeeper{}
	accKeeper := mockAccKeeper{}
	nftKeeper := mockNFTKeeper{
		classes:         make(map[string]*nft.Class),
		nfts:            make(map[string]*nft.NFT),
		nftsWithClassID: make(map[string]*nft.NFT),
	}
	priceFeedKeeper := mockPriceFeedKeeper{}
	incentiveKeeper := FakeIncentiveKeeper{
		poolIncentive: make(map[string]sdk.Coins),
	}
	bankKeeper := mockbankKeeper{make(map[string]sdk.Coins)}
	auctionKeeper := MockAuctionKeeper{
		AuctionAmount: make([]sdk.Coin, 1),
		SellerBid:     make([]string, 2),
	}

	k := keeper.NewKeeper(
		cdc,
		storeKey,
		memStoreKey,
		paramsSubspace,
		kycKeeper,
		bankKeeper,
		accKeeper,
		nftKeeper,
		priceFeedKeeper,
		auctionKeeper,
		incentiveKeeper,
	)

	ctx := sdk.NewContext(stateStore, tmproto.Header{}, false, log.NewNopLogger())

	// Initialize params
	k.SetParams(ctx, types.DefaultParams())

	if !k.IsHookSet() {
		k.SetHooks(&fakeSPVFunctions{})
	}

	return k, &nftKeeper, &bankKeeper, auctionKeeper, incentiveKeeper, ctx
}
