syntax = "proto3";
package joltify.third_party.incentive.v1beta1;

import "cosmos/base/v1beta1/coin.proto";
import "gogoproto/gogo.proto";
import "google/api/annotations.proto";
import "joltify/third_party/incentive/v1beta1/claims.proto";
import "joltify/third_party/incentive/v1beta1/params.proto";

option go_package = "github.com/joltify-finance/joltify_lending/x/third_party/incentive/types";

// Query defines the gRPC querier service for incentive module.
service Query {
  // Params queries module params.
  rpc Params(QueryParamsRequest) returns (QueryParamsResponse) {
    option (google.api.http).get = "/joltify/third_party/incentive/v1beta1/params";
  }

  // Rewards queries reward information for a given user.
  rpc Rewards(QueryRewardsRequest) returns (QueryRewardsResponse) {
    option (google.api.http).get = "/joltify/third_party/incentive/v1beta1/rewards";
  }

  // Rewards queries the reward factors.
  rpc RewardFactors(QueryRewardFactorsRequest) returns (QueryRewardFactorsResponse) {
    option (google.api.http).get = "/joltify/third_party/incentive/v1beta1/reward_factors";
  }

  rpc SPVRewards(QuerySPVRewardsRequest) returns (QuerySPVRewardsResponse) {
    option (google.api.http).get = "/joltify/third_party/incentive/v1beta1/spv_rewards";
  }

}

// QueryParamsRequest is the request type for the Query/Params RPC method.
message QueryParamsRequest {}

// QueryParamsResponse is the response type for the Query/Params RPC method.
message QueryParamsResponse {
  Params params = 1 [(gogoproto.nullable) = false];
}

// QueryRewardsRequest is the request type for the Query/Rewards RPC method.
message QueryRewardsRequest {
  // owner is the address of the user to query rewards for.
  string owner = 1;
  // reward_type is the type of reward to query rewards for, e.g. hard, earn,
  // swap.
  string reward_type = 2;
  // unsynchronized is a flag to query rewards that are not simulated for reward
  // synchronized for the current block.
  bool unsynchronized = 3;
}

// QueryRewardsResponse is the response type for the Query/Rewards RPC method.
message QueryRewardsResponse {

  repeated JoltLiquidityProviderClaim jolt_liquidity_provider_claims = 1 [
    (gogoproto.castrepeated) = "JoltLiquidityProviderClaims",
    (gogoproto.nullable) = false
  ];

  repeated SwapClaim swap_claims = 4 [
    (gogoproto.castrepeated) = "SwapClaims",
    (gogoproto.nullable) = false
  ];

  SPVIncentiveClaim  spv_incentive_claim = 5 [
    (gogoproto.castrepeated) = "SPVClaims",
    (gogoproto.nullable) = false
  ];

}

// QueryRewardFactorsRequest is the request type for the Query/RewardFactors RPC method.
message QueryRewardFactorsRequest {}

// QueryRewardFactorsResponse is the response type for the Query/RewardFactors RPC method.
message QueryRewardFactorsResponse {
  repeated MultiRewardIndex jolt_supply_reward_factors = 1 [
    (gogoproto.castrepeated) = "MultiRewardIndexes",
    (gogoproto.nullable) = false
  ];
  repeated MultiRewardIndex jolt_borrow_reward_factors = 2 [
    (gogoproto.castrepeated) = "MultiRewardIndexes",
    (gogoproto.nullable) = false
  ];

  repeated MultiRewardIndex swap_reward_factors = 3 [
    (gogoproto.castrepeated) = "MultiRewardIndexes",
    (gogoproto.nullable) = false
  ];

  repeated MultiRewardIndex spv_reward_factors = 4 [
    (gogoproto.castrepeated) = "MultiRewardIndexes",
    (gogoproto.nullable) = false
  ];
}

message QuerySPVRewardsRequest {
  string pool_index = 1;
  string owner = 2;
}

message QuerySPVRewardsResponse {
  repeated cosmos.base.v1beta1.Coin claimable_rewards = 1
  [(gogoproto.castrepeated) = "github.com/cosmos/cosmos-sdk/types.Coins", (gogoproto.nullable) = false];
}
